<?php

if ( ! defined( 'ABSPATH' ) ) exit;

// Register Custom Post Type
function krch_register_course() {

	$labels = array(
		'name'                => _x( 'Courses', 'Post Type General Name', 'krch' ),
		'singular_name'       => _x( 'Course', 'Post Type Singular Name', 'krch' ),
		'menu_name'           => __( 'Courses', 'krch' ),
		'parent_item_colon'   => __( 'Parent Item:', 'krch' ),
		'all_items'           => __( 'All Items', 'krch' ),
		'view_item'           => __( 'View Item', 'krch' ),
		'add_new_item'        => __( 'Add New Item', 'krch' ),
		'add_new'             => __( 'Add New', 'krch' ),
		'edit_item'           => __( 'Edit Item', 'krch' ),
		'update_item'         => __( 'Update Item', 'krch' ),
		'search_items'        => __( 'Search Item', 'krch' ),
		'not_found'           => __( 'Not found', 'krch' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'krch' ),
	);
	$args = array(
		'label'               => __( 'krch_courses', 'krch' ),
		'description'         => __( 'Courses', 'krch' ),
		'labels'              => $labels,
		'supports'            => array( 'title','editor'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 20,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite' => array('slug' => 'recognition-courses'),
	);
	register_post_type( 'krch_course', $args );

	$labels = array(
		'name'                       => _x( 'Course tags', 'Taxonomy General Name', 'krch' ),
		'singular_name'              => _x( 'Course tag', 'Taxonomy Singular Name', 'krch' ),
		'menu_name'                  => __( 'Course tags', 'krch' ),
		'all_items'                  => __( 'All Items', 'krch' ),
		'parent_item'                => __( 'Parent Item', 'krch' ),
		'parent_item_colon'          => __( 'Parent Item:', 'krch' ),
		'new_item_name'              => __( 'New Item Name', 'krch' ),
		'add_new_item'               => __( 'Add New Item', 'krch' ),
		'edit_item'                  => __( 'Edit Item', 'krch' ),
		'update_item'                => __( 'Update Item', 'krch' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'krch' ),
		'search_items'               => __( 'Search Items', 'krch' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'krch' ),
		'choose_from_most_used'      => __( 'Choose from the most used items', 'krch' ),
		'not_found'                  => __( 'Not Found', 'krch' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'krch_course_tag', array( 'krch_course' ), $args );

}
add_action( 'init', 'krch_register_course', 0 );
