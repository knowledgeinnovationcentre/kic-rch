<?php

if ( ! defined( 'ABSPATH' ) ) exit;


add_action('template_redirect','krch_acf_form_head');
function krch_acf_form_head() {
	acf_form_head();
}

add_shortcode( 'krch-course-form', 'krch_frontend_form' );
function krch_frontend_form( $args ) {

	$defaults = array(
		'field_group' => '',
	);

	$args = wp_parse_args($args, $defaults);
	extract($args, EXTR_SKIP);

	$acf_args = array(
		'post_id' => 'new',
		'post_title' => true,
		'field_groups' => array( (int) $field_group ),
		'submit_value' => __('Submit course','krch'),
		'updated_message' => __('Thank you. The course information has been successfully submited for a review','krch'),
	);

	ob_start();

	acf_form( $acf_args );

	$output = ob_get_contents();
	ob_end_clean();

	return $output;
}


function krch_pre_save_post( $post_id )
{
	// Create a new post
	$post = array(
			'post_status'  => 'draft' ,
			'post_title'  => $_POST['fields']['field_5484675465708'] .' - '. $_POST['fields']['field_54846bac49a65'],
			'post_type'  => 'krch_course' ,
			);

	// insert the post
	$post_id = wp_insert_post( $post );

	// return the new ID
	return $post_id;
}

add_filter('acf/pre_save_post' , 'krch_pre_save_post' );
