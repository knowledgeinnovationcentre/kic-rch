=== Kic Recognition Clearinghouse ===
Contributors: senicar
Tags: sample, blueprint
Requires at least: 3.5
Tested up to: 4.0
Stable tag: 0.0.1



== Description ==

= Shortcodes =

`[krch-course-form]`


== Installation ==

1. Upload `kic-rch` folder to the `/wp-content/plugins/` directory


== Changelog ==

= 14.09.10 =
* Initial version
