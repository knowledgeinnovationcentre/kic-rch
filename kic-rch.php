<?php
/*
Plugin Name: Kic Recognition Clearinghouse
Description:
Plugin URL:
Version: 14.09.10
Author: Senicar
Author URI: http://senicar.net
*/

if ( ! defined( 'ABSPATH' ) ) exit;

/* -------------------------------------------------------*
** CONST
** -------------------------------------------------------*/

define('KIC_RCH_PATH', plugin_dir_path(__FILE__));
define('KIC_RCH_URL', plugin_dir_url(__FILE__));


/*
|-------------------------------------------------------
| Advanced custom fields
|-------------------------------------------------------
*/

include_once('lib/advanced-custom-fields/acf.php');
include_once('lib/acf-repeater/acf-repeater.php');

//define( 'ACF_LITE', true );


/* -------------------------------------------------------*
** REQUIRE
** -------------------------------------------------------*/

require_once 'inc/course-cpt.php';
require_once 'inc/course-frontend-form.php';
//require_once 'inc/acf-custom-fields.php';


/* -------------------------------------------------------*
** Repository
** -------------------------------------------------------*/

// use Kic Repository (kic-repo) plugin for updates and Readme access
add_action('kic_repo_init', 'krch_add_repo');
function krch_add_repo($kic_repo) {
	$krch_repo = new Kic_Repository(__FILE__);
}
